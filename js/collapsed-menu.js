$(document).ready(function () {
    $('#header, .top-menu, #pendragon-mobile-logo, #pendragon-mobile-text').addClass('hide');
    $('.menu').css('display', 'block!important');
    $('.menu').addClass('collapse-menu').css({ transition: '1s' });
    $('.menu-button').addClass('collapse-menu-button').css({ transition: '1s' });
    $('.mega-menu').addClass('mega-menu-collapsed');
    $('#home-button, #blog-button-menu').show();
});