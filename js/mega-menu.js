$(document).ready(function () {

    //Hide elements by default
    $('#home-button').hide();
    $('#blog-button-menu').hide();

    //Highlight selected div
    $('.menu-button').mouseenter(function () {
        $(this).toggleClass('button-selected');
        $(".menu-button").not(this).css('background-color', 'white');
        $(".menu-button").not(this).css('color', 'black');
    });

    //COLLAPSED MENU BUTTON CLICK
    $('#home-button').click(function () {
        location.href = "http://pendragon.online";
    });

    //Close open menu sections on mouse leave
    $('.mega-menu').mouseleave(function () {
        $('.mega-menu').slideUp();
        $('.menu-button').addClass('unset-bg');
    });

    //Close open menu on page tap (for tablets)
    $('.parallax-section, #textcontainer').click(function () {
        $('.mega-menu').slideUp();
    });

    //Portfolio
    $('#portfolio-button').on('click', function (e) {
        $('.mega-menu:not(#portfolio-mega-menu)').slideUp();
        $('#portfolio-mega-menu').slideToggle();
        $('#portfolio-mega-menu').css('display', 'block');
    });

    //Work Experience
    $('#work-button').on('click', function (e) {
        $('.mega-menu:not(#work-mega-menu)').slideUp();
        $('#work-mega-menu').slideToggle();
        $('#work-mega-menu').css('display', 'block');
    });

    //About me
    $('#about-button').on('click', function (e) {
        $('.mega-menu:not(#about-mega-menu)').slideUp();
        $('#about-mega-menu').slideToggle();
        $('#about-mega-menu').css('display', 'block');
    });


    //MOBILE

    //Test to see if mobile menu is hidden
    var menuHidden = 0;

    $('#burger-button').click(function () {
        if (menuHidden === 0) {
            $('.photography-menu-button').css('display', 'none'); /*hide the photography menu on the photography pages*/
            $('#mobile-menu').slideDown();
            menuHidden = 1;
        }

        else if (menuHidden === 1) {
            $('.photography-menu-button').css('display', 'block'); /*show the photography menu on the photography pages*/
            $('#mobile-menu').slideUp();
            menuHidden = 0;
        }
    });

    //Tablet navigation
    if (matchMedia) {
        var query = window.matchMedia("(max-width: 1025px)");
        query.addListener(changeMenuText);
        changeMenuText(query);
    }

    function changeMenuText(query) {

        if (query.matches) {
            /*Change the 'University Blog' and 'Work Experience' text*/
            document.getElementById("work-button").innerHTML = "Work";
            document.getElementById("workexperience-footer").innerHTML = "Work";
        }

        else {
            //Change the button text back
            document.getElementById("work-button").innerHTML = "Work Experience";
            document.getElementById("workexperience-footer").innerHTML = "Work Experience";
        }
    }     
});
