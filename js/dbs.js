$(document).ready(function () {
    /*NOTICE: declare 'pageName' and 'pageIndex' in the in-line JS of the photography page*/
    
    $('.info-panel-slideshow').click(function () { //Hide all open info panels on panel click
        $(this).slideUp();
    });

    var photoNo = 1;
    var noOfImages = [13, 12, 15, 14, 14, 13, 12, 16, 8]; //index 0 = action, index 1 = architecture etc (in alphabetical order) - INDEX 8 = PHOTOGRAPHY PAGE
    
    $('.info-panel-slideshow:not(#slideshow-info-1)').css('display', 'none'); //Set first slides as active and open panels

    $('#dbs-next').click(function () {
        photoNo++; 
        checkPhotographyPage();
    });

    $('#dbs-prev').click(function () {
        photoNo--;
        checkPhotographyPage();
    });

    function checkPhotographyPage() {
        if (pageName !== 'photography') {
            loop();
            setURL();
        }
        else {
            photographyPage();
        }
    }

    function setURL() {
        var url = "../images/photography/" + pageName + "/" + pageName + "-" + photoNo + ".jpg";
        changeSlide(url);
    }

    function changeSlide(url) {  
        $('#dragonbase-slideshow').css("background-image", "url(" + url + ")");
        $('.info-panel-slideshow:not(#slideshow-info-' + photoNo + ')').slideUp();
        $('#slideshow-info-' + photoNo).slideToggle();
        $('#slideshow-info-' + photoNo).css('display', 'block');  
    }    

    function loop() {
        if (photoNo > noOfImages[pageIndex]) {
            photoNo = 1;
        }
        else if (photoNo < 1) {
            photoNo = noOfImages[pageIndex];
        }
    }

    function photographyPage() {
        photographyPageLoop();
        var themes = ['action', 'architecture', 'film', 'landscape', 'night', 'portrait', 'street', 'wildlife'];
        var url = "images/photography/" + themes[photoNo-1] + "/" + themes[photoNo-1] + "-3.jpg";
        changeSlide(url);
    }

    function photographyPageLoop() {
        if (photoNo === 0) {
            photoNo = 1;
        }
        if (photoNo > 8) {
            photoNo = 1;
        }
        if (photoNo < 1) {
            photoNo = 8;
        }
    }

    setInterval(function () { //automate slide advance every 5 seconds                                             
        photoNo++;
        checkPhotographyPage();                                                                            
    }, 5000);        
});