$(document).ready(function () {

    if (localStorage.getItem("ui-mode") === null) {                 //check to see if a cookie has been saved, if not then set the theme to light (where the cookie will be made, see line 34)
        setLight();
    }

    var savedtheme = localStorage.getItem("ui-mode");              //set the variable 'savedtheme' to get the contents of the cookie to determine the theme to use
    var currenttheme = "default";                                  //set the variable 'currenttheme' to 'default' - this value will be checked against to enable the toggling

    if (savedtheme === 'light') {                                  //this if statement sets the theme depending on the contents of the cookie (if a cookie exists)
        setLight();
    }
    else if (savedtheme === 'dark') {
        setDark();
    }
  
    $('.invert-button').click(function () {                         //when the user clicks on the invert button, the value of 'currenttheme' is checked to set the theme to the opposite one 
        if (currenttheme === "default") {                           //'currenttheme' is independent from the 'savedtheme' variable which checks the value of the theme that has been saved
            setDark();
        }
        else if (currenttheme === "dark") {
            setLight();
        }
    });

    function setLight() {
        $('html').removeClass("dark-ui");
        $('body, #textcontainer, .parallax-textbox, .menu, .menu-button').removeClass("dark-ui");
        $('.parallax-button').removeClass("dark-button");
        $('p, h4, .menu-button, li, i1').removeClass("white-text");
        $('.group-item').removeClass("red-text");
        $('#header').removeClass("dark-header");
        $('.footer').removeClass("dark-footer");
        localStorage.setItem("ui-mode", "light");                   //write 'light' to the cookie so that when the page is reloaded, the if statement (line 11) enables the light theme
        currenttheme = "default";                                   //change the value of 'currenttheme' as per usual toggle method
    }

    function setDark() {
        $('html').addClass("dark-ui");
        $('body, #textcontainer, .parallax-textbox, .menu, .menu-button').addClass("dark-ui");
        $('.parallax-button').addClass("dark-button");
        $('p, h4, .menu-button, li, i1').addClass("white-text");
        $('.group-item').addClass("red-text");
        $('#header').addClass("dark-header");
        $('.footer').addClass("dark-footer");
        localStorage.setItem("ui-mode", "dark");                    //write 'dark' to the cookie so that when the page is reloaded, the else if statement (line 14) enables the dark theme
        currenttheme = "dark";                                      //change the value of 'currenttheme' as per usual toggle method 
    }
    
});