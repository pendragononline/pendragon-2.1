$(window).scroll(function () {  

    if (matchMedia) {
        var mq = window.matchMedia("(min-width: 429px)");
        mq.addListener(WidthChange);
        WidthChange(mq);
    }

    function WidthChange(mq) {

        if (mq.matches) {
            /*These are enabled on devices 430px or wider*/
            if ($(this).scrollTop() > 0) {
                $('#header, .top-menu, #pendragon-mobile-logo, #pendragon-mobile-text').addClass('hide');
                $('.menu').css('display', 'block!important');
                $('.menu').addClass('collapse-menu').css({ transition: '1s' });
                $('.menu-button').addClass('collapse-menu-button').css({ transition: '1s' });
                $('.mega-menu').addClass('mega-menu-collapsed');
                $('#home-button, #blog-button-menu').show();
            }

            else {
                $('#header, .top-menu, #pendragon-mobile-logo, #pendragon-mobile-text').removeClass('hide');
                $('.menu').removeClass('collapse-menu').css({ transition: '1s' });
                $('.menu-button').removeClass('collapse-menu-button').css({ transition: '1s' });
                $('.mega-menu').removeClass('mega-menu-collapsed');
                $('#home-button, #blog-button-menu').hide();
            }
        }
    }
});